<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
                       xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml"
                       xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>Minutes Stayed</Name>
    <UserStyle>
      <Name>Minutes</Name>
      <Title>Minutes stayed</Title>
      <Abstract>Minutes stayed in one point in time</Abstract>
      <FeatureTypeStyle>
        <Rule>
          <Title>&lt; 1 min</Title>
          <ogc:Filter>
            <ogc:PropertyIsLessThan>
              <ogc:Div>
                <ogc:PropertyName>INTERVAL_MILLS</ogc:PropertyName>
                <ogc:Literal>60000</ogc:Literal>
              </ogc:Div>
              <ogc:Literal>1</ogc:Literal>
            </ogc:PropertyIsLessThan>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <!-- CssParameters allowed are fill (the color) and fill-opacity -->
              <CssParameter name="fill">#47e000</CssParameter>
              <CssParameter name="fill-opacity">0.7</CssParameter>
            </Fill>     
          </PolygonSymbolizer>
          <PointSymbolizer>

            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>

                  <!-- CssParameters allowed are fill (the color) and fill-opacity -->
                  <CssParameter name="fill">#47e000</CssParameter>
                  <CssParameter name="fill-opacity">0.7</CssParameter>

                </Fill>     
              </Mark>

              <Size>8</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>1 - 5 min</Title>
          <ogc:Filter>
            <ogc:PropertyIsBetween>
              <ogc:Div>
                <ogc:PropertyName>INTERVAL_MILLS</ogc:PropertyName>
                <ogc:Literal>60000</ogc:Literal>
              </ogc:Div>
              <ogc:LowerBoundary>
                <ogc:Literal>1</ogc:Literal>
              </ogc:LowerBoundary>
              <ogc:UpperBoundary>
                <ogc:Literal>5</ogc:Literal>
              </ogc:UpperBoundary>
            </ogc:PropertyIsBetween>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <!-- CssParameters allowed are fill (the color) and fill-opacity -->
              <CssParameter name="fill">#e1eb26</CssParameter>
              <CssParameter name="fill-opacity">0.7</CssParameter>
            </Fill>     
          </PolygonSymbolizer>
          <PointSymbolizer>

            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <!-- CssParameters allowed are fill (the color) and fill-opacity -->
                  <CssParameter name="fill">#e1eb26</CssParameter>
                  <CssParameter name="fill-opacity">0.7</CssParameter>

                  <CssParameter name="size">6</CssParameter>
                </Fill>     
              </Mark>
              <Size>22</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>&gt; 5 min</Title>
          <!-- like a linesymbolizer but with a fill too -->
          <ogc:Filter>
            <ogc:PropertyIsGreaterThan>
              <ogc:Div>
                <ogc:PropertyName>INTERVAL_MILLS</ogc:PropertyName>
                <ogc:Literal>60000</ogc:Literal>
              </ogc:Div>
              <ogc:Literal>5</ogc:Literal>
            </ogc:PropertyIsGreaterThan>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <!-- CssParameters allowed are fill (the color) and fill-opacity -->
              <CssParameter name="fill">#f20000</CssParameter>
              <CssParameter name="fill-opacity">0.7</CssParameter>
              <Size>
                <ogc:Literal>6</ogc:Literal>
              </Size>
            </Fill>     
          </PolygonSymbolizer>
          <PointSymbolizer>

            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>

                  <!-- CssParameters allowed are fill (the color) and fill-opacity -->
                  <CssParameter name="fill">#f20000</CssParameter>
                  <CssParameter name="fill-opacity">0.7</CssParameter>

                </Fill>  
              </Mark>
              <Size>32</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>