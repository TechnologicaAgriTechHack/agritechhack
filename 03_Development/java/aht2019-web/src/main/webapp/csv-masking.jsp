<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="b" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AHT2019|masking</title>
    </head>
    <body>
        <b:breadcrumb>
            <jsp:attribute name="pathLabels">welcome,*.csv masking</jsp:attribute>
            <jsp:attribute name="pathLinks">index.jsp,</jsp:attribute>
        </b:breadcrumb>
        <p>Number (scale up/down), date(scale up/down), longitude/latitude/elevation (move, toplo symmetry)</p>
        <form action="" method="POST">
            <b:csvinput/>
            <fieldset>
                <legend>.csv masking</legend>
                <table >
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>type</th>
                        <th>formula</th>
                    </tr> 
                    <!-- ts,lat,lon,spn,value,isoblue_id,spn_label-->
                    <tr> 
                        <td>1</td>
                        <td>ts</td>
                        <td>timestamp</td>
                        <td><input value="trim(?,'day')"/></td>
                    </tr>
                    <tr> 
                        <td>2</td>
                        <td>lat</td>
                        <td>latitude</td>
                        <td><input value="shift(?,42)"/></td>
                    </tr>
                    <tr> 
                        <td>3</td>
                        <td>lon</td>
                        <td>longitude</td>
                        <td><input value="shift(?,-42)"/></td>
                    </tr>
                    <tr> 
                        <td>4</td>
                        <td>spn</td>
                        <td>string</td>
                        <td> <input value=""/></td>
                    </tr>
                    <tr> 
                        <td>5</td>
                        <td>value</td>
                        <td>number</td>
                        <td> <input value="(x/7)*7"/></td>
                    </tr>
                </table>
            </fieldset>
            <b:csvoutput/>
            <p>
                <input type="submit"/>
            </p>
        </form>
    </body>
</html>
