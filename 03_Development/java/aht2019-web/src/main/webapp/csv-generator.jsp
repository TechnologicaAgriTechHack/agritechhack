<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="b" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AHT2019|generator</title>
    </head>
    <body>
        <b:breadcrumb>
            <jsp:attribute name="pathLabels">welcome,*.csv generator</jsp:attribute>
            <jsp:attribute name="pathLinks">index.jsp,</jsp:attribute>
        </b:breadcrumb>
        <p>How many row? How many columns? What types and pattern? Name tempate with bindings...</p>
        <form action="" method="POST"> 
            <fieldset>
                <legend>generator</legend>
                <table > 
                    <tr>
                        <th>row-count</th>
                        <td><input type="text" value="10000"/></td> 
                    </tr>
                    <tr>
                        <th></th>  
                        <td><input type="text" value="[a-z0-9]{2,3}" size="128"/></td>
                    </tr> 
                </table>
            </fieldset>
            <b:csvoutput/>
            <p>
                <input type="submit"/>
            </p>
        </form>
    </body>
</html>
