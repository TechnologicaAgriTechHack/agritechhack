<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="b" %>
<!DOCTYPE html>
<html>
    <head>
        <title>AHT2019|welcome</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <b:breadcrumb>
            <jsp:attribute name="pathLabels">welcome</jsp:attribute>
            <jsp:attribute name="pathLinks"></jsp:attribute>
        </b:breadcrumb>
        <h3>Demos for stream processing of .csv files</h3>

        <ul>
            <li><a href="csv-generator.jsp">*.csv generator</a></li>
            <li><a href="csv-masking.jsp">*.csv masking</a>
                <ul>
                    <li>numbers (x/5*3,...)</li>
                    <li>strings (hmac-md5,dictionary,...)</li>
                    <li>longitude/latitude/elevation</li>
                </ul>
            </li>
        </ul>

    </body>
</html>
