<fieldset>
    <legend>.csv input</legend>
    <table >
        <tr>
            <th>format</th>
            <td>
                <select>
                    <option>Default</option>
                    <option>Excel</option>
                    <option>InformixUnload</option>
                    <option>InformixUnloadCsv</option>
                    <option>MySQL</option>
                    <option>PostgreSQLCsv</option>
                    <option>PostgreSQLText</option>
                    <option>RFC4180</option>
                    <option>TDF</option>
                </select>
            </td>  
        </tr>
        <tr>
            <th>file/content</th>
            <td>
                <input type="file"/>
            </td>
        </tr>
    </table>
</fieldset>