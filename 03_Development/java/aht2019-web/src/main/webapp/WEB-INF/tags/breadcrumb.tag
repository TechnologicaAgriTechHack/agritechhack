<%@tag description="body-header" pageEncoding="UTF-8" %> 
<%@attribute name="pathLabels"%>
<%@attribute name="pathLinks"%>
<%@attribute name="subLabels"%>
<%@attribute name="subLinks"%>
<%{%>
<p>
    <%
        String[] pathLabelArray = (pathLabels == null || pathLabels.trim().isEmpty() ? "" : pathLabels).split("[,]");
        String[] pathLinkArray = (pathLinks == null || pathLinks.trim().isEmpty() ? "" : pathLinks).split("[,]");
        // преминат път
        for (int iLabel = 0; iLabel < pathLabelArray.length; iLabel++) {
            String pathLabel = org.apache.commons.text.StringEscapeUtils.escapeHtml4(pathLabelArray[iLabel]);
            String pathLink = pathLinkArray.length <= iLabel ? "" : org.apache.commons.text.StringEscapeUtils.escapeHtml4(pathLinkArray[iLabel]);
            if (pathLink.trim().isEmpty()) {
    %>
    &nbsp;/&nbsp;<%= pathLabel%>
    <%
    } else {
    %>
    &nbsp;/&nbsp;<a href="<%= pathLink%>"><%= pathLabel%></a>
    <%
            }
        }

        String[] subLabelArray = (subLabels == null || subLabels.trim().isEmpty() ? "" : subLabels).split("[,]");
        String[] subLinkArray = (subLinks == null || subLinks.trim().isEmpty() ? "" : subLinks).split("[,]");
        // директни наследници
        if (subLabels != null && !subLabels.trim().isEmpty()) {
    %>[<%
        String delim = "";
        for (int iLabel = 0; iLabel < subLabelArray.length; iLabel++) {
            String pathLabel = org.apache.commons.text.StringEscapeUtils.escapeHtml4(subLabelArray[iLabel]);
            String pathLink = subLinkArray.length <= iLabel ? "" : org.apache.commons.text.StringEscapeUtils.escapeHtml4(subLinkArray[iLabel]);
            if (pathLink.trim().isEmpty()) {
    %>
    <%= delim%><%= pathLabel%>
    <%
    } else {
    %>
    <%= delim%><a href="<%= pathLink%>"><%= pathLabel%></a></span>
    <%
            }
            delim = ",&nbsp;";
        }

    %>]<%        }
%>
</p>
<hr/>
<%}%>