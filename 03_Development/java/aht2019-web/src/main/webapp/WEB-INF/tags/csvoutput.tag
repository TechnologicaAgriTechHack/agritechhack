<fieldset>
    <legend>.csv output</legend>
    <table >
        <tr>
            <th>format</th>
            <td>
                <select>
                    <option>Default</option>
                    <option>Excel</option>
                    <option>InformixUnload</option>
                    <option>InformixUnloadCsv</option>
                    <option>MySQL</option>
                    <option>PostgreSQLCsv</option>
                    <option>PostgreSQLText</option>
                    <option>RFC4180</option>
                    <option>TDF</option>
                </select>
            </td>  
        </tr>
        <tr>
            <th>name</th>
            <td>
                <input value="r&#x24;{row-count}c&#x24;{row-count}.&#x24;{timestamp}.csv" size="64"/>
            </td>
        </tr>
    </table>
</fieldset>