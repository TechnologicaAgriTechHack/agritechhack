/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.helper.csv.upload;

import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Arrays;

/**
 * еднократно качвам файловете с обработени J1939 данни от менторите
 *
 * @author cstoykov
 */
public class UploadData {

    private static String QUERY = "insert into "
            + "RAW_CSV(TS,LAT,LON,SPN,SPN_LABEL,VALUE) "
            + "values( "
            + "to_date(?,'yyyy-mm-dd hh24:mi:ss'),/*TS*/ "
            + "?,/*LAT*/ "
            + "?,/*LON*/ "
            + "?,/*SPN*/ "
            + "?,/*SPN_LABEL*/ "
            + "?/*VALUE*/ "
            + ")";

    public static void main(String... args) throws Exception {

        String[] files = {
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Actual Percent Torque (513) 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Engine Fuel Rate (183) 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Engine RPM (190) 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Engine RPM 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Rear PTO Output Shaft Speed (1883) 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Rear PTO Output Shaft Speed Setpoint (1885) 10-06-2019.csv",
            "F:\\Projects\\prj-internal-components\\AgriTechHack2019\\01_Analysis\\data\\Dataset Wheel-based Vehicle Speed (84) 10-06-2019.csv"
        };
        Class.forName("oracle.jdbc.driver.OracleDriver");
        try (Connection ora = DriverManager.getConnection("jdbc:oracle:thin:@192.168.22.80:1521:ORCL", "ATH2019", "ATH2019ATH2019");) {
            ora.setAutoCommit(false);
            try (PreparedStatement ps = ora.prepareStatement(QUERY)) {
                for (String file : files) {
                    try (CSVReader csvr = new CSVReader(new FileReader(new File(file)))) {
                        String[] line = null;
                        //0 ts,
                        //1 lat,
                        //2 lon,
                        //3 spn,
                        //4 value,
                        //5 isoblue_id,
                        //6 spn_label
                        csvr.readNext();
                        int count = 0;
                        while ((line = csvr.readNext()) != null) {
                            try {
                                ps.clearParameters();
                                int iParameter = 1;
                                ps.setString(iParameter++, line[0]);
                                ps.setObject(iParameter++, new BigDecimal(line[1]));
                                ps.setObject(iParameter++, new BigDecimal(line[2]));
                                ps.setString(iParameter++, line[3]);
                                ps.setString(iParameter++, line[6]);
                                ps.setObject(iParameter++, new BigDecimal(line[4]));
                                ps.executeUpdate();
                                count += 1;
                                if (count >= 10000) {
                                    count = 0;
                                    ora.commit();
                                }
                            } catch (Exception e) {
                                System.out.println(file);
                                System.out.println(Arrays.toString(line));
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                }
            }
            ora.commit();
        }
    }
}
