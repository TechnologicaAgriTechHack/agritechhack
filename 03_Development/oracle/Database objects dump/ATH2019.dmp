prompt PL/SQL Developer Export User Objects for user ATH2019@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.22.80)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SID=ORCL)))
prompt Created by gkolev on 14 ��������� 2019 �.
set define off
spool ATH2019.log

prompt
prompt Creating table F_FILES
prompt ======================
prompt
create table ATH2019.F_FILES
(
  id                VARCHAR2(64) default rawtohex(sys_guid()) not null,
  machines_url_id   VARCHAR2(64) not null,
  date_loaded       DATE default sysdate not null,
  file_content      BLOB not null,
  file_name         VARCHAR2(256),
  file_mimetype     VARCHAR2(256),
  file_characterset VARCHAR2(256),
  file_length       NUMBER
)
;
comment on column ATH2019.F_FILES.id
  is 'PK, UUID';

prompt
prompt Creating table F_LINES_LAYER
prompt ============================
prompt
create table ATH2019.F_LINES_LAYER
(
  id         VARCHAR2(64) default rawtohex(sys_guid() ) not null,
  f_lines_id VARCHAR2(64) not null,
  geom       SDO_GEOMETRY
)
;
create index ATH2019.F_LINES_LAYER_SIDX on ATH2019.F_LINES_LAYER (GEOM)
  indextype is MDSYS.SPATIAL_INDEX;

prompt
prompt Creating table F_MACHINES
prompt =========================
prompt
create table ATH2019.F_MACHINES
(
  id                          VARCHAR2(64) default rawtohex(sys_guid()) not null,
  code                        VARCHAR2(32) not null,
  name                        VARCHAR2(1024),
  year_of_production          NUMBER,
  engine_type                 VARCHAR2(32),
  turbo                       CHAR(1),
  drive_type                  CHAR(10),
  horse_powers                NUMBER,
  color                       VARCHAR2(4000),
  dimensions_length           NUMBER,
  dimensions_width            NUMBER,
  dimensions_wheelbase        NUMBER,
  dimensions_ground_clearance NUMBER,
  weight                      NUMBER,
  fuel_capacity               NUMBER,
  photo                       BLOB,
  id_number                   NUMBER not null,
  pulling_capacity            NUMBER
)
;
comment on column ATH2019.F_MACHINES.id
  is 'PK, UUID';
comment on column ATH2019.F_MACHINES.code
  is 'UK';
comment on column ATH2019.F_MACHINES.name
  is 'name, descrption';
alter table ATH2019.F_MACHINES
  add constraint F__PK primary key (ID);
alter table ATH2019.F_MACHINES
  add constraint F_MACHINES_DT
  check (DRIVE_TYPE in ('2WD','4WD'));
alter table ATH2019.F_MACHINES
  add constraint F_MACHINES_ET
  check (ENGINE_TYPE in ('FUEL','ELECTRIC'));
alter table ATH2019.F_MACHINES
  add constraint F_MACHINES_T
  check (TURBO in ('Y','N'));

prompt
prompt Creating table F_SPECIFICATIONS
prompt ===============================
prompt
create table ATH2019.F_SPECIFICATIONS
(
  id      VARCHAR2(64) default rawtohex(sys_guid()) not null,
  version VARCHAR2(32) not null,
  code    VARCHAR2(32) not null
)
;
comment on column ATH2019.F_SPECIFICATIONS.id
  is 'PK, UUID';
comment on column ATH2019.F_SPECIFICATIONS.version
  is 'version';
comment on column ATH2019.F_SPECIFICATIONS.code
  is 'spec code';
alter table ATH2019.F_SPECIFICATIONS
  add constraint F_SPECIFICATIONS_UK primary key (ID);

prompt
prompt Creating table F_MACHINES_URLS
prompt ==============================
prompt
create table ATH2019.F_MACHINES_URLS
(
  id         VARCHAR2(64) default rawtohex(sys_guid()) not null,
  machine_id VARCHAR2(64) not null,
  spec_id    VARCHAR2(64) not null,
  url        VARCHAR2(4000) not null
)
;
comment on column ATH2019.F_MACHINES_URLS.id
  is 'PK, UUID';
comment on column ATH2019.F_MACHINES_URLS.machine_id
  is 'machine id';
comment on column ATH2019.F_MACHINES_URLS.spec_id
  is 'specification id';
comment on column ATH2019.F_MACHINES_URLS.url
  is 'source address';
alter table ATH2019.F_MACHINES_URLS
  add constraint F_MACHINES_URLS_PK primary key (ID);
alter table ATH2019.F_MACHINES_URLS
  add constraint F_MACHINES_M_FK foreign key (MACHINE_ID)
  references ATH2019.F_MACHINES (ID);
alter table ATH2019.F_MACHINES_URLS
  add constraint F_MACHINES_S_FK foreign key (SPEC_ID)
  references ATH2019.F_SPECIFICATIONS (ID);

prompt
prompt Creating table F_POLILINES
prompt ==========================
prompt
create table ATH2019.F_POLILINES
(
  id             VARCHAR2(64) default rawtohex(sys_guid() ) not null,
  machine_id     VARCHAR2(64) not null,
  x              NUMBER,
  y              NUMBER,
  interval_mills NUMBER,
  df             TIMESTAMP(6),
  dt             TIMESTAMP(6),
  line_id        VARCHAR2(64),
  point_no       NUMBER
)
;
comment on column ATH2019.F_POLILINES.interval_mills
  is 'DT-DF � ���������';
comment on column ATH2019.F_POLILINES.line_id
  is 'unique line id';
comment on column ATH2019.F_POLILINES.point_no
  is 'sequence for unique line';
alter table ATH2019.F_POLILINES
  add constraint F_MACHINE_POLILINES_PK primary key (ID);

prompt
prompt Creating table F_POLILINES_LAYER
prompt ================================
prompt
create table ATH2019.F_POLILINES_LAYER
(
  id             VARCHAR2(64) default rawtohex(sys_guid() ) not null,
  f_polilines_id VARCHAR2(64) not null,
  geom           SDO_GEOMETRY
)
;
create index ATH2019.F_POLILINES_LAYER_SIDX on ATH2019.F_POLILINES_LAYER (GEOM)
  indextype is MDSYS.SPATIAL_INDEX;
alter table ATH2019.F_POLILINES_LAYER
  add constraint F_POLILINES_LAYER_PK primary key (ID);

prompt
prompt Creating table F_SPECIFICATION_DETAIL
prompt =====================================
prompt
create table ATH2019.F_SPECIFICATION_DETAIL
(
  id                 VARCHAR2(64) default rawtohex(sys_guid()) not null,
  f_specification_id VARCHAR2(64) not null,
  code               VARCHAR2(32) not null,
  sensor_id_address  VARCHAR2(4000) not null
)
;
comment on column ATH2019.F_SPECIFICATION_DETAIL.id
  is 'PK, UUID';
comment on column ATH2019.F_SPECIFICATION_DETAIL.f_specification_id
  is 'SPEC ID';
comment on column ATH2019.F_SPECIFICATION_DETAIL.code
  is 'code';
comment on column ATH2019.F_SPECIFICATION_DETAIL.sensor_id_address
  is 'sensor id address';
alter table ATH2019.F_SPECIFICATION_DETAIL
  add constraint F_SPECIFICATION_DETAIL_PK primary key (ID);
alter table ATH2019.F_SPECIFICATION_DETAIL
  add constraint F_SPECIFICATION_DETAIL_FS_FK foreign key (F_SPECIFICATION_ID)
  references ATH2019.F_SPECIFICATIONS (ID);

prompt
prompt Creating table F_SPECIFICATION_DETAIL_SUBSCR
prompt ============================================
prompt
create table ATH2019.F_SPECIFICATION_DETAIL_SUBSCR
(
  id                        VARCHAR2(64) default rawtohex(sys_guid()) not null,
  f_specification_detail_id VARCHAR2(64) not null,
  substr_start_pos          NUMBER not null,
  substr_stop_pos           NUMBER not null,
  description               VARCHAR2(4000) not null,
  units                     VARCHAR2(32) not null,
  scale                     NUMBER not null,
  offset                    NUMBER not null
)
;
comment on table ATH2019.F_SPECIFICATION_DETAIL_SUBSCR
  is 'spec detail subscriptions';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.id
  is 'PK, UUID';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.f_specification_detail_id
  is 'ID for specification detail';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.substr_start_pos
  is 'Substring start position';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.substr_stop_pos
  is 'Substring stop position';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.description
  is 'description';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.units
  is 'units (kg, km, m, deg, etc.)';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.scale
  is 'scale for formula';
comment on column ATH2019.F_SPECIFICATION_DETAIL_SUBSCR.offset
  is 'offset for formula';
alter table ATH2019.F_SPECIFICATION_DETAIL_SUBSCR
  add constraint F_SPECIFICATION_DETAIL_SUBSCR_UK primary key (ID);
alter table ATH2019.F_SPECIFICATION_DETAIL_SUBSCR
  add constraint F_SPECIFICATION_DETAIL_SUBSCR_FSD_FK foreign key (F_SPECIFICATION_DETAIL_ID)
  references ATH2019.F_SPECIFICATION_DETAIL (ID);

prompt
prompt Creating table RAW_CSV
prompt ======================
prompt
create table ATH2019.RAW_CSV
(
  ts        DATE,
  lat       NUMBER,
  lon       NUMBER,
  spn       VARCHAR2(128),
  spn_label VARCHAR2(256),
  value     NUMBER
)
;

prompt
prompt Creating sequence DEPT_SEQ
prompt ==========================
prompt
create sequence ATH2019.DEPT_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 50
increment by 1
cache 20;

prompt
prompt Creating sequence EMP_SEQ
prompt =========================
prompt
create sequence ATH2019.EMP_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 8000
increment by 1
cache 20;

prompt
prompt Creating sequence F_MACHINES_SEQ
prompt ================================
prompt
create sequence ATH2019.F_MACHINES_SEQ
minvalue 1
maxvalue 99999999999999999999999
start with 12
increment by 1
cache 20;

prompt
prompt Creating view V_LINES_LAYER
prompt ===========================
prompt
create or replace force view ath2019.v_lines_layer as
select r.id, r.f_lines_id, r.geom
  from f_lines_layer r
 where r.f_lines_id in (select line_id from f_polilines r where r.machine_id = 'Tracktor10');

prompt
prompt Creating view V_POLILINES_LAYER
prompt ===============================
prompt
create or replace force view ath2019.v_polilines_layer as
select f.df, f.dt, f.id, f.interval_mills, f.line_id, f.machine_id, f.point_no, l.geom  from F_POLILINES f join F_POLILINES_LAYER l on l.F_POLILINES_ID = f.id
where f.machine_id='Tracktor10';

prompt
prompt Creating package ATH_GEOM_UTILS
prompt ===============================
prompt
CREATE OR REPLACE PACKAGE ATH2019.ATH_GEOM_UTILS AS


    /*Gets distance in meters between 2 points by coordinates */
    function GET_DISTANCE_BETWEEN_GEOMETRIES_COORDS(P_FIRST_LAT NUMBER, P_FIRST_LON NUMBER, P_SECOND_LAT NUMBER , P_SECOND_LON NUMBER) return NUMBER;

    /*Gets distance in meters between 2 points by WKT */
    function GET_DISTANCE_BETWEEN_GEOMETRIES_WKT(P_FIRST_PINT VARCHAR2, P_SECOND_POINT VARCHAR2) return NUMBER;

    /*Gets distance in meters between 2 points by SDO_GEOM */
    function GET_DISTANCE_BETWEEN_GEOMETRIES(P_POINT_A MDSYS.SDO_GEOMETRY, P_POINT_B MDSYS.SDO_GEOMETRY) return NUMBER;

END ATH_GEOM_UTILS;
/

prompt
prompt Creating package DATA
prompt =====================
prompt
create or replace package ath2019.DATA is

  procedure start_load(p_tolerance number, p_machine_id varchar2);
  procedure add(p_x number, p_y number, p_df timestamp);
  procedure end_load;
  procedure load_data(p_tolerance number, p_machine_id varchar2);
  procedure create_lines;
end DATA;
/

prompt
prompt Creating package body ATH_GEOM_UTILS
prompt ====================================
prompt
CREATE OR REPLACE PACKAGE BODY ATH2019.ATH_GEOM_UTILS AS

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
  function GET_DISTANCE_BETWEEN_GEOMETRIES_COORDS(P_FIRST_LAT NUMBER, P_FIRST_LON NUMBER, P_SECOND_LAT NUMBER , P_SECOND_LON NUMBER) return NUMBER IS
    point1 MDSYS.SDO_GEOMETRY;
    point2 MDSYS.SDO_GEOMETRY;
  begin   
    point1 := SDO_GEOMETRY(2001, 8307, SDO_POINT_TYPE(P_FIRST_LAT, P_FIRST_LON, NULL),NULL,NULL);
    point2 := SDO_GEOMETRY(2001, 8307, SDO_POINT_TYPE(P_SECOND_LAT, P_SECOND_LON, NULL),NULL,NULL);
    return GET_DISTANCE_BETWEEN_GEOMETRIES (point1, point2);
  end GET_DISTANCE_BETWEEN_GEOMETRIES_COORDS;
  
  function GET_DISTANCE_BETWEEN_GEOMETRIES_WKT(P_FIRST_PINT VARCHAR2, P_SECOND_POINT VARCHAR2) return NUMBER IS
    point1 MDSYS.SDO_GEOMETRY;
    point2 MDSYS.SDO_GEOMETRY;
  begin   
    point1 := SDO_GEOMETRY(P_FIRST_PINT,8307);
    point2 := SDO_GEOMETRY(P_SECOND_POINT, 8307);
    return GET_DISTANCE_BETWEEN_GEOMETRIES (point1, point2);
  end GET_DISTANCE_BETWEEN_GEOMETRIES_WKT;
  
  function GET_DISTANCE_BETWEEN_GEOMETRIES(P_POINT_A MDSYS.SDO_GEOMETRY, P_POINT_B MDSYS.SDO_GEOMETRY) return NUMBER IS
  begin
        return SDO_GEOM.SDO_DISTANCE (P_POINT_A, P_POINT_B, 0.005,'unit=m');
  end GET_DISTANCE_BETWEEN_GEOMETRIES;
  
  
END ATH_GEOM_UTILS;
/

prompt
prompt Creating package body DATA
prompt ==========================
prompt
create or replace package body ath2019.DATA is

  l_line_id       varchar2(64);
  l_point_no      number;
  l_tolerance     number;
  l_machine_id    varchar2(64);
  l_previous_geom sdo_geometry;

  l_prev_date_from timestamp;
  l_previous_id    varchar2(64);

  /** Start load process */
  procedure start_load(p_tolerance number, p_machine_id varchar2) is
  begin
    l_machine_id    := p_machine_id;
    l_tolerance     := p_tolerance;
    l_point_no      := 1;
    l_line_id       := rawtohex(sys_guid());
    l_previous_geom := null;
  end;

  /** Add line */
  procedure add(p_x number, p_y number, p_df timestamp) is
  begin
    declare
      wkt         varchar2(256);
      id          varchar2(64);
      geom        sdo_geometry;
      start_dt_ms number;
    begin
      wkt  := 'POINT (' || p_x || ' ' || p_y || ')';
      geom := SDO_GEOMETRY(wkt, 4326);
      if l_previous_geom is null then
        id := rawtohex(sys_guid());
        -- store prev data
        l_previous_id    := id;
        l_previous_geom  := geom;
        l_prev_date_from := p_df;
      
        -- insert sa_data
        INSERT INTO F_POLILINES
          (ID, MACHINE_ID, X, Y, DF, LINE_ID, POINT_NO)
        VALUES
          (id, l_machine_id, p_x, p_y, p_df, l_line_id, l_point_no);
        -- insert geom data
        INSERT INTO F_POLILINES_LAYER
          (ID, F_POLILINES_ID, GEOM)
        VALUES
          (rawtohex(sys_guid()), id, l_previous_geom);
      else
        l_point_no := l_point_no + 1;
        if ATH_GEOM_UTILS.GET_DISTANCE_BETWEEN_GEOMETRIES(l_previous_geom,
                                                          geom) >=
           l_tolerance then
        
          select sum((extract(hour from p_df) -
                     extract(hour from l_prev_date_from)) * 3600 +
                     (extract(minute from p_df) -
                     extract(minute from l_prev_date_from)) * 60 +
                     extract(second from p_df) -
                     extract(second from l_prev_date_from)) * 1000 ms
            into start_dt_ms
            from dual;
        
          update f_polilines
             set dt = p_df, interval_mills = start_dt_ms
           where id = l_previous_id;
        
          id := rawtohex(sys_guid());
          -- store prev data
          l_previous_id    := id;
          l_prev_date_from := p_df;
          l_previous_geom  := geom;
          -- insert sa_data
          INSERT INTO F_POLILINES
            (ID, MACHINE_ID, X, Y, DF, LINE_ID, POINT_NO)
          VALUES
            (id, l_machine_id, p_x, p_y, p_df, l_line_id, l_point_no);
          -- insert geom data
          INSERT INTO F_POLILINES_LAYER
            (ID, F_POLILINES_ID, GEOM)
          VALUES
            (rawtohex(sys_guid()), id, l_previous_geom);
        
        end if;
      end if;
    
    end;
  
  end;

  /** End load process */
  procedure end_load is
  begin
    UPDATE F_POLILINES SET DT = sysdate WHERE id = l_previous_id;
  end;

  /** Initiate load process for the demo data */
  procedure load_data(p_tolerance number, p_machine_id varchar2) is
  begin
    begin
      -- Call the procedure
      data.start_load(p_tolerance  => p_tolerance,
                      p_machine_id => p_machine_id);
      declare
        CURSOR c_raw IS
          select t.ts, t.lat, t.lon, t.spn, t.spn_label, t.value
            from raw_csv t
          -- too much data, get the first 200000, remove in prod
           where rownum < 200000
           order by t.ts asc;
      begin
        for rec in c_raw loop
          data.add(rec.lon, rec.lat, rec.ts);
        
          null;
        end loop;
      end;
      data.end_load();
    end;
  end;
  /** Create polylines from the data, if holes are found - create multiple lines */
  procedure create_lines is
  begin
    INSERT INTO f_lines_layer (id, f_lines_id, geom)
    SELECT rawtohex(sys_guid()),
           line_id,
           linestring
      FROM (SELECT c.line_id,
                   mdsys.sdo_geometry(2002,
                                      4326,
                                      NULL,
                                      mdsys.sdo_elem_info_array(1, 2, 1),
                                      CAST(MULTISET
                                           (SELECT b.COLUMN_VALUE
                                              FROM f_polilines a,
                                                   TABLE(mdsys.sdo_ordinate_array(a.x,
                                                                                  a.y)) b
                                             WHERE a.line_id = c.line_id
                                             ORDER BY a.point_no, rownum) AS
                                           mdsys.sdo_ordinate_array)) AS linestring
              FROM f_polilines c
             GROUP BY c.line_id
             ORDER BY c.line_id) f;
  
  end;

end DATA;
/


prompt Done
spool off
set define on
