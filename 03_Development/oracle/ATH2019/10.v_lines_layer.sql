create or replace view v_lines_layer as
select r.id, r.f_lines_id, r.geom
  from f_lines_layer r
 where r.f_lines_id in (select line_id from f_polilines r where r.machine_id = 'Tracktor10')
