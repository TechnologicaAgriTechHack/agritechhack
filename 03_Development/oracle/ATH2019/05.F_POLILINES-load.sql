declare
  v_code varchar2(256);
  v_id varchar2(256);
  v_x number;
  v_y number;
  v_step number;
  v_df date;
  v_dt date;
  v_line_id varchar2(256);
begin
  for i in 1..10 loop
     v_code := 'TRACTOR/'||lpad(i,3,'0');
     v_line_id := sys_guid();
     select id into v_id from F_MACHINES where code = v_code;
     v_x := 23.923180;
     v_y := 43.706268;
     v_df := sysdate;
     v_dt := v_df;
     v_step := 0.001;
     for r in (
     -----------------------------
     select * from(
        select rownum no, dx, dy, round(dbms_random.value(1,10))*1/24/60 dd/*min*/, round(dbms_random.value(1,10))*1/24/60 dt/*min*/
        from(
        select  sign(round(dbms_random.value(-2,2)))  dx, sign(round(dbms_random.value(-2,2))) dy
        from (
        select *
        from dual connect by dummy = dummy
        )
        )where dx != 0 or dy != 0
     ) where rownum<=100
     -----------------------------
     ) loop
       -- next 
       v_x := v_x + r.dx*v_step;
       v_y := v_y + r.dy*v_step;
       v_df:= v_df + r.dt;
       v_dt:= v_df + r.dd; 
       -- 
       insert into F_POLILINES(MACHINE_ID,X,Y,INTERVAL_MILLS,DF,DT,Line_ID,POINT_NO)
       values (v_id,v_x,v_y,(v_dt-v_df)*1000,v_df,v_dt,v_line_id,r.no);
       
     end loop;
  end loop;
  commit;
end;