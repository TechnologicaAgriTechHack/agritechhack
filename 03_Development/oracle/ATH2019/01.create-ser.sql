-- USER SQL
ALTER USER "ATH2019"
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP"
ACCOUNT UNLOCK ;

-- QUOTAS

-- ROLES
ALTER USER "ATH2019" DEFAULT ROLE "DBA","CONNECT","RESOURCE";

-- SYSTEM PRIVILEGES

