select'delete from user_sdo_geom_metadata where table_name = '''|| t.tname||''' and column_name = '''|| t.cname ||''';'
from col t
where t.coltype = '"MDSYS"."SDO_GEOMETRY"'
      and t.tname not like '%$%'
union all
select'insert into user_sdo_geom_metadata VALUES ( '''|| t.tname||''','''|| t.cname ||''',SDO_DIM_ARRAY( SDO_DIM_ELEMENT(''Long'', -180, 180, 0.005), SDO_DIM_ELEMENT(''Lat'', -90, 90, 0.005) ), 4326);'
from col t
where t.coltype = '"MDSYS"."SDO_GEOMETRY"'
            and t.tname not like '%$%'
union all

select  'DROP INDEX '||t.tname||'_SIDX;'
from col t
where t.coltype = '"MDSYS"."SDO_GEOMETRY"'
      and t.tname not like '%$%'
      and t.tname not like 'V_%'
union all

select  'CREATE INDEX '||t.tname||'_SIDX  ON '||t.tname||'('||t.cname||')INDEXTYPE IS MDSYS.SPATIAL_INDEX;'
from col t
where t.coltype = '"MDSYS"."SDO_GEOMETRY"'
      and t.tname not like 'V_%'
      and t.tname not like '%$%'
