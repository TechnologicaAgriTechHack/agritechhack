create or replace package DATA is

  procedure start_load(p_tolerance number, p_machine_id varchar2);
  procedure add(p_x number, p_y number, p_df timestamp);
  procedure end_load;
  procedure load_data(p_tolerance number, p_machine_id varchar2);
  procedure create_lines;
end DATA;
/
create or replace package body DATA is

  l_line_id       varchar2(64);
  l_point_no      number;
  l_tolerance     number;
  l_machine_id    varchar2(64);
  l_previous_geom sdo_geometry;

  l_prev_date_from timestamp;
  l_previous_id    varchar2(64);

  /** Start load process */
  procedure start_load(p_tolerance number, p_machine_id varchar2) is
  begin
    l_machine_id    := p_machine_id;
    l_tolerance     := p_tolerance;
    l_point_no      := 1;
    l_line_id       := rawtohex(sys_guid());
    l_previous_geom := null;
  end;

  /** Add line */
  procedure add(p_x number, p_y number, p_df timestamp) is
  begin
    declare
      wkt         varchar2(256);
      id          varchar2(64);
      geom        sdo_geometry;
      start_dt_ms number;
    begin
      wkt  := 'POINT (' || p_x || ' ' || p_y || ')';
      geom := SDO_GEOMETRY(wkt, 4326);
      if l_previous_geom is null then
        id := rawtohex(sys_guid());
        -- store prev data
        l_previous_id    := id;
        l_previous_geom  := geom;
        l_prev_date_from := p_df;
      
        -- insert sa_data
        INSERT INTO F_POLILINES
          (ID, MACHINE_ID, X, Y, DF, LINE_ID, POINT_NO)
        VALUES
          (id, l_machine_id, p_x, p_y, p_df, l_line_id, l_point_no);
        -- insert geom data
        INSERT INTO F_POLILINES_LAYER
          (ID, F_POLILINES_ID, GEOM)
        VALUES
          (rawtohex(sys_guid()), id, l_previous_geom);
      else
        l_point_no := l_point_no + 1;
        if ATH_GEOM_UTILS.GET_DISTANCE_BETWEEN_GEOMETRIES(l_previous_geom,
                                                          geom) >=
           l_tolerance then
        
          select sum((extract(hour from p_df) -
                     extract(hour from l_prev_date_from)) * 3600 +
                     (extract(minute from p_df) -
                     extract(minute from l_prev_date_from)) * 60 +
                     extract(second from p_df) -
                     extract(second from l_prev_date_from)) * 1000 ms
            into start_dt_ms
            from dual;
        
          update f_polilines
             set dt = p_df, interval_mills = start_dt_ms
           where id = l_previous_id;
        
          id := rawtohex(sys_guid());
          -- store prev data
          l_previous_id    := id;
          l_prev_date_from := p_df;
          l_previous_geom  := geom;
          -- insert sa_data
          INSERT INTO F_POLILINES
            (ID, MACHINE_ID, X, Y, DF, LINE_ID, POINT_NO)
          VALUES
            (id, l_machine_id, p_x, p_y, p_df, l_line_id, l_point_no);
          -- insert geom data
          INSERT INTO F_POLILINES_LAYER
            (ID, F_POLILINES_ID, GEOM)
          VALUES
            (rawtohex(sys_guid()), id, l_previous_geom);
        
        end if;
      end if;
    
    end;
  
  end;

  /** End load process */
  procedure end_load is
  begin
    UPDATE F_POLILINES SET DT = sysdate WHERE id = l_previous_id;
  end;

  /** Initiate load process for the demo data */
  procedure load_data(p_tolerance number, p_machine_id varchar2) is
  begin
    begin
      -- Call the procedure
      data.start_load(p_tolerance  => p_tolerance,
                      p_machine_id => p_machine_id);
      declare
        CURSOR c_raw IS
          select t.ts, t.lat, t.lon, t.spn, t.spn_label, t.value
            from raw_csv t
          -- too much data, get the first 200000, remove in prod
           where rownum < 200000
           order by t.ts asc;
      begin
        for rec in c_raw loop
          data.add(rec.lon, rec.lat, rec.ts);
        
          null;
        end loop;
      end;
      data.end_load();
    end;
  end;
  /** Create polylines from the data, if holes are found - create multiple lines */
  procedure create_lines is
  begin
    INSERT INTO f_lines_layer (id, f_lines_id, geom)
    SELECT rawtohex(sys_guid()),
           line_id,
           linestring
      FROM (SELECT c.line_id,
                   mdsys.sdo_geometry(2002,
                                      4326,
                                      NULL,
                                      mdsys.sdo_elem_info_array(1, 2, 1),
                                      CAST(MULTISET
                                           (SELECT b.COLUMN_VALUE
                                              FROM f_polilines a,
                                                   TABLE(mdsys.sdo_ordinate_array(a.x,
                                                                                  a.y)) b
                                             WHERE a.line_id = c.line_id
                                             ORDER BY a.point_no, rownum) AS
                                           mdsys.sdo_ordinate_array)) AS linestring
              FROM f_polilines c
             GROUP BY c.line_id
             ORDER BY c.line_id) f;
  
  end;

end DATA;
/
