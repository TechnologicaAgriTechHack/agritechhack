--------------------------------------------------------
--  File created - Saturday-September-14-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package ATH_GEOM_UTILS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "ATH2019"."ATH_GEOM_UTILS" AS 


    /*Gets distance in meters between 2 points by coordinates */
    function GET_DISTANCE_BETWEEN_GEOMETRIES_COORDS(P_FIRST_LAT NUMBER, P_FIRST_LON NUMBER, P_SECOND_LAT NUMBER , P_SECOND_LON NUMBER) return NUMBER;

    /*Gets distance in meters between 2 points by WKT */
    function GET_DISTANCE_BETWEEN_GEOMETRIES_WKT(P_FIRST_PINT VARCHAR2, P_SECOND_POINT VARCHAR2) return NUMBER;

    /*Gets distance in meters between 2 points by SDO_GEOM */
    function GET_DISTANCE_BETWEEN_GEOMETRIES(P_POINT_A MDSYS.SDO_GEOMETRY, P_POINT_B MDSYS.SDO_GEOMETRY) return NUMBER;

END ATH_GEOM_UTILS;

/
