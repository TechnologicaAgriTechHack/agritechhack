--------------------------------------------------------
--  File created - Friday-September-13-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table F_POLILINES_LAYER
--------------------------------------------------------

  CREATE TABLE "ATH2019"."F_POLILINES_LAYER" 
   (	"ID" VARCHAR2(64 BYTE) DEFAULT rawtohex(sys_guid() ), 
	"F_POLILINES_ID" VARCHAR2(64 BYTE), 
	"GEOM" "SDO_GEOMETRY"
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" 
 VARRAY "GEOM"."SDO_ELEM_INFO" STORE AS SECUREFILE LOB 
  ( TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192
  CACHE  NOCOMPRESS  KEEP_DUPLICATES ) 
 VARRAY "GEOM"."SDO_ORDINATES" STORE AS SECUREFILE LOB 
  ( TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192
  CACHE  NOCOMPRESS  KEEP_DUPLICATES ) ;
--------------------------------------------------------
--  DDL for Index F_POLILINES_LAYER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ATH2019"."F_POLILINES_LAYER_PK" ON "ATH2019"."F_POLILINES_LAYER" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table F_POLILINES_LAYER
--------------------------------------------------------

  ALTER TABLE "ATH2019"."F_POLILINES_LAYER" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ATH2019"."F_POLILINES_LAYER" MODIFY ("F_POLILINES_ID" NOT NULL ENABLE);
  ALTER TABLE "ATH2019"."F_POLILINES_LAYER" ADD CONSTRAINT "F_POLILINES_LAYER_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
