export class WmsDefModel {
  public url: string;
  public layerName: string;
  public title: string;
  public visibility: boolean;
  public opacity: number;
  public selectable: boolean;
  public params: any;

  constructor(input: Partial<WmsDefModel>) {
    if (input) {
      this.url = input.url;

      this.layerName = input.layerName;
      this.title = input.title;
      this.visibility = input.visibility;
      this.opacity = input.opacity;
      this.selectable = input.selectable;
      this.params = input.params;

      if (!this.params) {
        this.params = {};
      }
    }
  }
}
