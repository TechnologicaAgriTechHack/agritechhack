import OlBaseLayer from 'ol/layer/Base';
import OlImage from 'ol/layer/Image';
import OlTileLayer from 'ol/layer/Tile';
import OlProjection from 'ol/proj/Projection';
import OlImageWMS from 'ol/source/ImageWMS';
import OlTileWMS from 'ol/source/TileWMS';
import OlXYZ from 'ol/source/XYZ';
import OlTileGrid from 'ol/tilegrid/TileGrid';
import { WmsDefModel } from '../models/WmsDef.model';

export class MapService {
  private _resolutions = [
    156543.03390625, // 0
    78271.516953125, // 1
    39135.7584765625, // 2
    19567.87923828125, // 3
    9783.939619140625, // 4
    4891.9698095703125, // 5
    2445.9849047851562, // 6
    1222.9924523925781, // 7
    611.4962261962891, // 8
    305.74811309814453, // 9
    152.87405654907226, // 10
    76.43702827453613, // 11
    38.218514137268066, // 12
    19.109257068634033, // 13
    9.554628534317017 // 14
  ];
  get resolutions(): number[] {
    return this._resolutions;
  }

  private _extent = [
    26.978793575679088,
    43.58719564054423,
    26.98312435084384,
    43.59055404678083
  ];
  get extent(): number[] {
    return this._extent;
  }

  /**
   * Retrieves untiled WMS map
   *
   * @param wmsDef WMS definition
   */
  getUntiledMap(wmsDef: WmsDefModel) {
    const resultLayer = new OlImage({
      source: new OlImageWMS({
        ratio: 1.1,
        url: 'http://192.168.22.80:8090/geoserver/agri/wms',
        params: {
          FORMAT: 'image/png',
          VERSION: '1.1.1',
          STYLES: '',
          LAYERS: wmsDef.layerName
        }
      })
    });

    if (wmsDef.opacity) {
      resultLayer.setOpacity(wmsDef.opacity);
    }
    if (wmsDef.layerName) {
      resultLayer.set('name', wmsDef.layerName);
    }
    if (wmsDef.title) {
      resultLayer.set('title', wmsDef.title);
    }
    if (wmsDef.selectable) {
      resultLayer.set('selectable', wmsDef.selectable);
    }

    resultLayer.setVisible(wmsDef.visibility);

    this.addLayerSourceEvents(resultLayer);
    return resultLayer;
  }

  public getBackgroundLayer() {
    var tiled = new OlTileLayer({
      source: new OlXYZ({
        url: 'http://{1-4}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png'
      })
    });
    return tiled;
  }

  addLayerSourceEvents(layer: OlBaseLayer) {
    layer.getSource().on('imageloadend', event => {
      layer.set('imageloaderror', false);
    });
    layer.getSource().on('imageloaderror', event => {
      layer.set('imageloaderror', true);
    });
  }

  getTiledMap(wmsDef: WmsDefModel): OlTileLayer {
    const paramsArr = {
      FORMAT: 'image/png8',
      VERSION: '1.1.1',
      LAYERS: wmsDef.layerName,
      STYLES: '',
      TILED: true
      // 'WIDTH': 256,
      // 'HEIGHT': 256
    };

    const proj = new OlProjection({
      code: 'EPSG:3857',
      units: 'm',
      axisOrientation: 'enu'
    });

    const tileGrid = new OlTileGrid({
      extent: this.extent,
      resolutions: this.resolutions,
      tileSize: [256, 256]
    });

    const resultLayer = new OlTileLayer(
      {
        source: new OlTileWMS({
          url: wmsDef.url,
          params: paramsArr,
          crossOrigin: 'Anonymous',
          serverType: 'geoserver',
          projection: proj,
          tileGrid: tileGrid,
          hidpi: false
        })
      },
      {
        tileOptions: { crossOriginKeyword: 'anonymous' },
        transitionEffect: null
      }
    );

    if (wmsDef.opacity) {
      resultLayer.setOpacity(wmsDef.opacity);
    }

    if (wmsDef.layerName) {
      resultLayer.set('name', wmsDef.layerName);
    }

    if (wmsDef.title) {
      resultLayer.set('title', wmsDef.title);
    }

    if (wmsDef.selectable) {
      resultLayer.set('selectable', wmsDef.selectable);
    }

    resultLayer.setVisible(wmsDef.visibility);
    return resultLayer;
  }

  public getMainWMSDefinition(): WmsDefModel {
    return new WmsDefModel({
      url: 'http://192.168.22.80:8090/geoserver/agri/wms',
      layerName: 'agri:V_POLILINES_LAYER',
      title: 'MAIN',
      visibility: true,
      opacity: 0.65,
      selectable: false
    });
  }
}
