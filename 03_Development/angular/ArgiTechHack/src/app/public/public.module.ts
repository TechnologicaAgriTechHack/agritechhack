import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './components/map/map.component';
import { LayoutComponent } from './components/layout/layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { MapService } from './services/map.service';

@NgModule({
  declarations: [MapComponent, LayoutComponent],
  imports: [CommonModule, RouterModule, BrowserModule],
  providers: [MapService]
})
export class PublicModule {}
