import { Component, OnInit } from '@angular/core';
import OlMap from 'ol/Map';
import OlView from 'ol/View';
import Overlay from 'ol/Overlay.js';
import { defaults } from 'ol/control';
import { MapService } from '../../services/map.service';
import Projection from 'ol/proj/Projection';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  mapWidth = 800;
  mapHeight = 600;

  hasZoom = true;
  hasPan = true;
  map: OlMap = null;

  overlay: Overlay;
  private projObject: any = null;
  private readonly projCode: string = 'EPSG:32635';

  constructor(private mapService: MapService) {
    this.projObject = new Projection({
      code: this.projCode,
      units: 'm',
      axisOrientation: 'enu', //neu; enu
      global: false
    });
  }

  ngOnInit() {
    this.createMap();
  }

  private createMap() {

    
    const background = this.createBackGroundLayer();
    const pointLayer = this.createPointLayer();
    const lineLayer = this.createLineLayer();

    const format = 'image/png';
    const bounds = [5.590364494, 52.531997322, 5.602251102, 52.542457053];
    const proj = new Projection({
      code: 'EPSG:4326',
      units: 'degrees',
      global: false
    });
    this.map = new OlMap({
      controls: defaults({
        attribution: false
      }),
      target: 'map',
      layers: [background, lineLayer, pointLayer],
      view: new OlView({
        projection: proj
      })
    });
    this.map.getView().fit(bounds, this.map.getSize());
  }

  private createPointLayer(): any {
    const tmpLayer = this.mapService.getUntiledMap(
      this.mapService.getMainWMSDefinition()
    );
    return tmpLayer;
  }

  public createLineLayer(): any {
    const definition = this.mapService.getMainWMSDefinition();
    definition.layerName = 'agri:V_LINES_LAYER';
    const tmpLayer = this.mapService.getUntiledMap(definition);
    return tmpLayer;
  }

  public createBackGroundLayer() {
    const tmpLayer = this.mapService.getBackgroundLayer();
    return tmpLayer;
  }

  doSearch() {
    console.log('do search');
  }
}
